from channels.routing import ChannelNameRouter, ProtocolTypeRouter
from fabriclive.consumers import BackgroundTaskConsumer

application = ProtocolTypeRouter(
    {"channel": ChannelNameRouter({"background-tasks": BackgroundTaskConsumer,})}
)
