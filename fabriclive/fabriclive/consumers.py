from time import sleep
from channels.consumer import SyncConsumer

from projects.models import HostGroup
from projects.service import run_host_group


class BackgroundTaskConsumer(SyncConsumer):
    def run_host_group(self, message):
        host_group = HostGroup.objects.get(id=message["host_group_id"])
        run_host_group(host_group, message["task"])

    def task_b(self, message):
        sleep(message["wait"])
