from projects.models import HostGroup
from projects.runner import run_fabric


def run_host_group(host_group: HostGroup, task: str):
    project = host_group.project
    hosts = list(host_group.host_set.values_list("hostname", flat=True))
    run_fabric(
        project.runner.command, task, project.run_file, "/root/dev.key.pub", hosts
    )
