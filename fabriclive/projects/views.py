import random
from django.http import HttpResponse
from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from projects.models import Project
from projects.service import run_host_group

channel_layer = get_channel_layer()


def index(request, pk=None):
    proj = Project.objects.get(pk=pk)
    hg = proj.hostgroup_set.first()
    # run_host_group(hg, "status")
    # async_to_sync(channel_layer.send)("background-tasks", {"type": "task_a", "id": id})
    id = random.randint(0, 1000)
    async_to_sync(channel_layer.send)(
        "background-tasks",
        {"type": "run_host_group", "task": "status", "host_group_id": hg.id},
    )
    return HttpResponse(
        "task_a message sent with id={}".format(id), content_type="text/plain"
    )
