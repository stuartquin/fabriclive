from django.db import models
from projects.runner import parse_fabfile


class Runner(models.Model):
    command = models.CharField(max_length=512)
    description = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.command


class Project(models.Model):
    name = models.CharField(max_length=512)
    created_at = models.DateTimeField(auto_now_add=True)
    runner = models.ForeignKey(Runner, models.SET_NULL, blank=True, null=True)
    run_file = models.TextField(blank=True, null=True)
    tasks = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    def save(self):
        self.tasks = parse_fabfile(self.run_file)
        super().save()


class HostGroup(models.Model):
    name = models.CharField(max_length=512)
    project = models.ForeignKey(Project, models.SET_NULL, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.project.name} {self.name}"


class Host(models.Model):
    hostname = models.CharField(max_length=512)
    host_group = models.ForeignKey(HostGroup, models.SET_NULL, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.hostname


class Deploy(models.Model):
    host_group = models.ForeignKey(HostGroup, models.SET_NULL, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)


class RunStatus(models.Model):
    status = models.CharField(max_length=512)


class Run(models.Model):
    deploy = models.ForeignKey(Deploy, models.SET_NULL, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    status = models.ForeignKey(RunStatus, models.SET_NULL, blank=True, null=True)
