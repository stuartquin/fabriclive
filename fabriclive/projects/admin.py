from django.contrib import admin
from projects import models

admin.site.register(models.Project)
admin.site.register(models.HostGroup)
admin.site.register(models.Host)
admin.site.register(models.Deploy)
