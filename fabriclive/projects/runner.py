import subprocess
import tempfile

from typing import List


def save_fabfile(content: str) -> str:
    tmp_dir = tempfile.mkdtemp()
    path = f"{tmp_dir}/fabfile.py"

    with open(path, "w") as fabfile:
        fabfile.write(content)

    return tmp_dir


def run_fabric(run_cmd: str, task: str, content: str, identity: str, hosts: List[str]):
    tmp_dir = save_fabfile(content)
    run_hosts = ",".join(hosts)
    cmd = [run_cmd, "-i", identity, "-H", run_hosts, task]

    print(" ".join(cmd))
    subprocess.run(cmd, cwd=tmp_dir)


def _parse_arguments(args: str):
    content = args.replace("Arguments:", "").strip()
    return [a.strip() for a in content.split(",") if a.strip()]


def _run_process(cmd: List[str], tmp_dir: str) -> str:
    completed_process = subprocess.run(
        cmd, cwd=tmp_dir, encoding="utf8", stdout=subprocess.PIPE,
    )

    return completed_process.stdout


def parse_fab3(tmp_dir: str):
    output = _run_process(["fab", "-l"], tmp_dir)
    lines = output.split("\n")
    commands = [l.strip().split(" ")[0] for l in lines[2:] if l.strip()]

    tasks = {}
    for command in commands:
        lines = _run_process(["fab", "-d", command], tmp_dir).split("\n")[2:]
        help_text = ""
        cmd = {}

        for line in lines:
            content = line.strip()
            if content.startswith("Arguments:"):
                cmd["arguments"] = _parse_arguments(content)
            elif content:
                help_text = f"{help_text}\n{content}"

        cmd["help_text"] = help_text.strip("\n")
        tasks[command] = cmd
    return tasks


def parse_fabfile(content: str):
    tmp_dir = save_fabfile(content)
    return parse_fab3(tmp_dir)
