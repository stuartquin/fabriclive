FROM python:3.7

RUN mkdir -p /app

ADD requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt
WORKDIR /app

ADD fabriclive /app/fabriclive
